import {
  Finger,
  FingerCurl,
  FingerDirection,
  GestureDescription,
} from "fingerpose";

const pointLeftDescription = new GestureDescription("pointLeft")

pointLeftDescription.addCurl(Finger.Index, FingerCurl.NoCurl, 1.0);
pointLeftDescription.addDirection(Finger.Index, FingerDirection.HorizontalLeft, 1.0);
pointLeftDescription.addDirection(
  Finger.Index,
  FingerDirection.DiagonalUpLeft,
  0.9
);
pointLeftDescription.addDirection(
  Finger.Index,
  FingerDirection.DiagonalDownLeft,
  0.9
);

for (let finger of [Finger.Thumb, Finger.Middle, Finger.Ring, Finger.Pinky]) {
  pointLeftDescription.addCurl(finger, FingerCurl.FullCurl, 1.0);
  pointLeftDescription.addCurl(finger, FingerCurl.HalfCurl, 0.25);
}

pointLeftDescription.addDirection(
  Finger.Thumb,
  FingerDirection.DiagonalUpLeft,
  1.0
);
pointLeftDescription.addDirection(
  Finger.Thumb,
  FingerDirection.VerticalUp,
  1.0
);
pointLeftDescription.addDirection(
  Finger.Thumb,
  FingerDirection.HorizontalLeft,
  1.0
);

export default pointLeftDescription;